function onClickHandler(){
    let inputEl = document.getElementById("inputText");
    let enterValue = inputEl.value;
    if(enterValue.trim() == ""){
        alert("invalid")
        return
    }
    let url = `https://api.github.com/search/users?q=${enterValue}`
    
    let html ="";
    
    fetch(url)
    .then(res => res.json())
    .then(data => {
        let arr= data.items;
        for(let obj of arr){
            html = html + "<tr><td>" + obj.login + "</td><td>" + `<img src="${obj.avatar_url}">'` + "</td></tr>";
        }
        document.getElementsByTagName("tbody")[0].innerHTML=html;
    })
}


