


// const ist = []

// const store = () => {
//     localStorage.setItem("users",JSON.stringify(ist))
// }

// const show = (users) => {
//     const rs = ist.map((u, index) => 
//     `<div><span>${u}</span><button dataIndex = "${index}">Delete</button></div>`
//     ).join('')
    
//     const userList = `<h3>Users</h3>${rs}`
//     document.getElementById("app").innerHTML = userList;
// }

// const addBtn = document.getElementById("add-btn");
// addBtn.addEventListener("click", () => {
//     const inputText = document.getElementById("inputData");
//     // console.log(res)
//     // console.log(inputText) 
//     ist.push(inputText.value)
//     store()
//     show(ist)
// })


// document.getElementById("app").addEventListener('click', res => {
//     res.stopPropagation();
//     if (res.target.tagName === 'BUTTON') {
//         const index = res.target.getAttribute('dataIndex')
//         const leftList = ist.slice(0, index)
//         const rightList = ist.slice(index+1, ist.length);
//         ist = [...leftList, ...rightList]
//         store();
//         show(ist)
//     }
// })

// const userFromStorage = localStorage.getItem('users');
// if(userFromStorage){
//     const userStored = JSON.parse(userFromStorage)
//     ist = userStored
//     show(userStored)
// }

const myInput = document.getElementById("inputData")
const addBtn = document.getElementById("add-btn")
const table = document.getElementById("app")

let users = [];

const storeToLocal = () => {
  localStorage.setItem('users', JSON.stringify(users));
};

const show = (users) => {
  const rowStr = users
    .map((u, index) => 
    `<div>
        <span>${u}</span>
        <button dataIndex="${index}">X</button>
    </div>`).join('');
  const userListStr = `<h3>Users</h3>${rowStr}`;
  table.innerHTML = userListStr;
};

addBtn.addEventListener('click', () => {
  const newUsername = myInput.value;
  if(newUsername.trim() === '') alert("invalid")
  else {
        users.push(newUsername);
        storeToLocal();
        show(users);
    }
});

table.addEventListener('click', (res) => {
    res.stopPropagation();
  if (res.target.tagName === 'BUTTON') {
    const indexOfItem = res.target.getAttribute('dataIndex');
    const leftSideList = users.slice(0, indexOfItem);
    const rightSideList = users.slice(indexOfItem + 1, users.length);
    users = [...leftSideList, ...rightSideList];
    storeToLocal();
    show(users);
  }
});

const nameFromLocal = localStorage.getItem('users');
if (nameFromLocal) {
  const names = JSON.parse(nameFromLocal);
  users = names;
  show(names);
}
